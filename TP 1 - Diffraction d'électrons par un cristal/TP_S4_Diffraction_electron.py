# -*- coding: utf-8 -*-
"""
Created on Sun Feb  7 18:05:38 2021

@author: Gabriel Amorosetti 
L2 PS Physique
"""

import numpy as np
import matplotlib.pyplot as plt

from scipy import stats


V_a=[2500,2700,2900,3100,3300,3500,3700,3900,4100,4300,4500,4700,4900,5100] 
# Tension d'accélération des électrons, en Volt (fournie par l'alimentation haute tension)

abscisses=np.array([0.0637728,0.06136540541,0.05921155831,0.05726966503,0.05550709769,0.05389785326,0.05242091869,0.05105910363,0.04979819041,0.04862630413,0.04753343864,0.04651109465,0.045552,0.04464989087])
# 3,18864/sqrt(V_a), abscisses pour tracer la courbe D=f(3,18864/sqrt(V_a))

delta_D=0.002 
# Incertitude sur la mesure des diamètres, en mètre

D_1=[0.031,0.03,0.029,0.027,0.027,0.026,0.025,0.024,0.024,0.023,0.023,0.022,0.022,0.022]
# Diamètre du 1er anneau, en mètre

D_2=[0.053,0.052,0.05,0.048,0.047,0.045,0.044,0.043,0.042,0.041,0.04,0.039,0.038,0.037]
# Diamètre du 2ème anneau, en mètre


#

reg_1=stats.linregress(abscisses,D_1)   #
reg_2=stats.linregress(abscisses,D_2)   #


#

plt.figure()

plt.plot(abscisses,D_1,'o', label='Mesures expérimentales')
plt.errorbar(abscisses, D_1, delta_D, None, 'none', capsize=3, label='Incertitude sur la mesure')
plt.plot(abscisses, reg_1.intercept + reg_1.slope*abscisses, 'r', label='Régression linéaire, R²={:4.3f}'.format(reg_1.rvalue**2))

plt.title('$ 1^{er}$ anneau')
plt.xlabel('$ \dfrac{3.18864}{\sqrt{V_{A}}} $')
plt.ylabel('Diamètre ($m$)')
plt.grid(True)
plt.text(0.05,0.029,'Coefficient directeur a={:5.4f}'.format(reg_1.slope), fontdict=None)


plt.legend()
plt.show()


#

plt.figure()

plt.plot(abscisses,D_2,'o', label='Mesures expérimentales')
plt.errorbar(abscisses, D_2, delta_D, None, 'none', capsize=3, label='Incertitude sur la mesure')
plt.plot(abscisses, reg_2.intercept + reg_2.slope*abscisses, 'r', label='Régression linéaire $(ax+b)$, R²={:4.3f}'.format(reg_2.rvalue**2))

plt.title('$ 2^{ème}$ anneau')
plt.xlabel('$ \dfrac{3.18864}{\sqrt{V_{A}}} $')
plt.ylabel('Diamètre ($m$)')
plt.grid(True)
plt.text(0.0485,0.0485,'Coefficient directeur a={:5.4f}'.format(reg_2.slope), fontdict=None)

plt.legend()
plt.show()



print('Coefficient directeur de la régression linéaire pour le 1er anneau, a =',reg_1.slope)
print('Coefficient directeur de la régression linéaire pour le 2ème anneau, a =',reg_2.slope)
